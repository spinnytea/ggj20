# GGJ20

Return to Star Cross - MUD, wander spaceship, solve puzzles, get it back to earth

The source file is `Starship.inform/Source/story.ni`

# Notes

## Scene setup text

Who is the player. Someone whoe doesn't know anything about the ship (beemed up? amnesia?) Maybe the opening text can describe amnesia. "the last thing you remember is an explosion before things go very very dark"

## Gameplay Notes

What story do we want to tell (we, the authors, describe).
What do we want the player to do (which keys to put in which locks, what buttons to press).

Each of the 4 rooms has a lock, each of the 4 rooms has a key.
* the Sickbay has it's own key and lock
* galley -> bridge -> engine room -> galley
	* Fix the engine
	* Pilot the ship (once the engine is fixed, then start it)

Sickbay
Galley (with vending machine)
Bridge (with red button)
Engine Room

# TODO

1. Is there anything we are missing? Brainstorm
1. playtest
1. playtest
1. prepare GGJ deliverable

# Resources
[Inform 7](http://inform7.com/)
