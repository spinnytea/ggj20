"Derelict" by JTK and Garrett
[ adjective: in a very poor condition as a result of disuse and neglect ]
[ noun: a person without a home, job, or property ]

Release along with a website, an interpreter.
Release along with an introductory postcard.

Include Inanimate Listeners by Emily Short.
Include Basic Help Menu by Emily Short.


Chapter 0 - Preamble

Understand "use [something]" as using.  Using is an action applying to one thing.  Carry out using: say "You will have to be more specific about your intentions."

Understand "use [door]" as going.

Understand "shake [something]" and "rock [something]" and "roll [something]" and "rattle [something]" and "bang on [something]" and "bang [something]" and "kick [something]" as attacking.

When play begins:
	choose row 1 in Table of Basic Help Options;
	now description entry is "You are a derelict on a derelict ship.  The ship needs to be repaired.  Explore the ship to find out what happened, and find a way to survive."

Calling for is an action applying to one topic.
Understand "yell [text]" and "yell for [text]" and "call [text]" and "call for [text]" and "shout [text]" and "shout for [text]" and "say [text]" as calling for.

Instead of calling for [text]:
	If the player is in Comm Space:
		try asking the Captain about it;
	otherwise:
		say "In space, no one can hear you...."

Yelling is an action applying to nothing.
Understand "yell"  and "call" and  "shout" and "scream" as yelling.

Instead of yelling:
	If the player is in Comm Space:
		say "'I can hear you quite well.  If you have something to say, just say it!'";
	otherwise:
		say "In space, no one can hear you...."

When play begins:
	say "[italic type]...what have you done...[roman type][paragraph break]";
	say "[italic type]...we're coming in too hot...[roman type][paragraph break]";
	say "The last thing you remember is an explosion before things go very very dark.[paragraph break]";
	say "[paragraph break][paragraph break]";

After printing the banner text, say "[paragraph break][paragraph break]"

The description of the player is "You are wearing a tattered jumpsuit with faded bloodstains.  You notice some healed scars on your arms and shoulders that you are pretty sure were not there before...".

The player is wearing a jumpsuit.  The description of the jumpsuit is "Your jumpsuit has seen better days.  There are what looks like bloodstains across your left arm and shoulders.  There is one bright spot where a badge used to be."

Instead of taking off the jumpsuit:
	say "Tattered as it is, the jumpsuit provides your only protection."

The bloodstains are part of the jumpsuit.  The description is "The bloodstains are old and faded...  and you are not even sure that they are yours."


Chapter 1 - The Sickbay

The Sickbay is a room.  "You survey the ruins of the sickbay, where injured crewmembers once sought a solace that is no longer present.  [if unvisited]You try to sit up, but you cannot move.  You look down and find yourself strapped to a stasis gurney."

The Sickbay Door is a closed door.  It is west of The Sickbay and east of the Hallway.  The description is "A door without a porthole.  It's probably meant for privacy."

The stasis gurney is a supporter in the Sickbay and fixed in place.  The description is "A battered but functional stasis gurney hums softly along one wall.  A silver tray juts from the side."  The player is on the stasis gurney.

The straps are a part of the stasis gurney.  Understand "strap" as the straps.  The straps can be cut or uncut.  The straps are uncut.  The description is "[if the straps are uncut]Tight nylon straps bind your legs, hips, and chest to the gurney.[otherwise]The straps hang loosely from the gurney."

The tray is a supporter and part of the stasis gurney.  The description is "A stainless steel tray juts from the side of the stasis gurney, designed to hold surgical tools."

The laser scalpel is a thing.  The description is "A precision laser scalpel.  [if straps are uncut]A green light glows dimly at the base -- it seems that it has some charge left.[otherwise]It seems to be out of power."

Before taking the laser scalpel:
	if the straps are uncut:
		say "You strain against the straps holding you in place.  With great effort, you snag the laser scalpel with your little finger and flip it onto your lap.  [run paragraph on]"

Before getting off the stasis gurney:
	if the straps are uncut:
		say "[one of]You try to stand, but you are strapped securely to the stasis gurney.[or]You tug at the straps, but they do not budge.  Maybe you need to take a closer look at your surroundings.[or]You strain against the straps with all your might, but to no avail.  (Try to [bold type]EXAMINE[roman type] or [bold type]LOOK AT[roman type] things around you.)[cycling]";
		stop the action.

Before pulling the straps:
	if the straps are uncut:
		instead try silently getting off the stasis gurney.

Before pulling the player:
	if the straps are uncut:
		instead try silently getting off the stasis gurney;
	otherwise:
		instead say "You are free!"

Understand "pull on [something]" as pulling.
Understand "unstrap [something]" or "undo [something]" as pulling.
Understand "sit up" as exiting.

Before examining the tray:
	if the laser scalpel is nowhere:
		now the laser scalpel is on the tray.

Instead of cutting the straps:
	if the player has the laser scalpel:
		say "You slice the straps with the laser scalpel.  They slide free and dangle from the gurney.";
		now the straps are cut;
	otherwise:
		say "You claw at the straps, but they hold tight."

Before opening the sickbay door:
	if the player is on the gurney:
		say "You cannot reach the door from the stasis gurney.";
		stop the action;
	otherwise:
		say "The door opens with a pneumatic hiss.  [run paragraph on]"

Before going through the Sickbay Door:
	if the Sickbay Door is closed:
		say "The Sickbay Door is closed.";
		stop the action.

Understand "exit [something]" as going.


Chapter 2 - The Bridge

The Bridge is a room.  "You are standing on the bridge, looking across empty duty stations.  Most are melted slag; however, one console was somehow spared complete destruction.  [if the communications pod is scenery]A darkened communications pod stands in a shadow in the corner.[otherwise]The communications pod is emitting enough light polution that makes it hard to see.  The rest of the room feels darker with it lit."

The Duty Stations are scenery in the bridge.  Understand "duty station" and "station" and "slag" and "melted slag" as the duty stations.  The description is "Melted slag.  Luckily no one was sitting there at the time, not that you'd really be able to tell."

A console is in the bridge.  "This console is still functional.  On its face is a prominent red button."  The console is fixed in place.  Understand "wires/screen/dials" as console.  The description is "Loose wires hang out of one corner.  [if warp drive is humming]The screens and dials are backlit, but they have no readouts.[otherwise]The screens and dials are matté and stale.  Only one button is lit."

The red button is part of the console.  The description is "[if warp drive is humming]The button has gone matté and stale.[else if tank is full]A strong solid glow.[otherwise]A faint pulsing glow."

After pushing the red button:
	if the tank is full:
		say "The ship purrs to life.";
		now the warp drive is humming;
		now the communications pod is open;
		now the communications pod is not scenery;
		now the console is scenery;
	otherwise:
		say "Something clicks, then ka-chunks, then whirls back down to sleep."


A computer is a kind of thing.  A computer is addressable.  Understand "computer" as a computer.  A computer has a table name called the contents.

Instead of consulting or asking a computer about a topic listed in the contents of the noun:
	say "[reply entry][paragraph break]";

Report consulting a computer about:
	instead say "You peruse through [the noun], but find no reference to [the topic understood].  The damage to the memory banks must be rather severe."

Table of Library Entries
topic	reply
"[candy bar]"	"The standard issue nutrition composite provides a satisfying source of energy for all crew members.  It is based upon a complex hydrocarbon matrix of natural and artificial ingredients."
"[warp engine]"	"The entry on the warp engine is incomplete, and most of what remains is too technical to comprehend.  The basic concept seems to be that it uses quantum fusion inside a complex hydrocarbon matrix to generate dark energy that does...  something involving too much math."
"[ship log]"	"The computer takes a long time to respond.  There's a sudden intake of air, and then the computer resets."
"[bridge]"	"The bridge of a ship is the room or platform from which the ship can be commanded.  When a ship is under way, the bridge is manned by an officer of the watch aided usually by an able seaman acting as lookout."
"[pilot]"	"What do you mean?  You are the pilot."
"[captain]"	"The captain and the crew have escaped."

Understand "candy" or "bar" or "candy bar" or "candybar" as "[candy bar]".
Understand "warp" or "engine" or "drive" or "warp engine" or "warp drive" as "[warp engine]".
Understand "logs" or "log" or "ship" or "ship's" or "ship logs" or "ship log" or "ship's log" or "ship's logs" as "[ship log]".
Understand "bridge" or "the bridge" as "[bridge]".
Understand "pilot" or "the pilot" or "a pilot" as "[pilot]".
Understand "captain" or "the captain" or "crew" or "the crew" or "wife" or "my wife" as "[captain]".

The Library Computer is a computer in the Bridge.  It is fixed in place.  The contents of it is the Table of Library Entries.  The description is "Half-hidden behind a fallen panel is a Library Computer.  Broken wires and cables hang from beneath.  A cursor blinks wanly on the screen."

Instead of using the Library Computer:
	say "The Library Computer contains knowlege about a galaxy's worth of items.  You can consult it about almost anything."

Understand "consult [something]" as using.


A communications pod is in the Bridge.  "[if the communications pod is open]A brightly lit communications pod invites you to enter.[otherwise]A floating egg that casts a long shadow."  It is an opaque closed enterable container and scenery.  A communications pod can be entered or unentered.  A communications pod is unentered.

After entering the communications pod:
	say "You climb into the communications pod.  [if pod is unentered]The door is small and it feels crampt.  As the egg comes to life, it appears to grow vast before your eyes.  [end if]The bridge fades away as comm stars whirl all around you.";
	now the communications pod is entered;
	if the player is not wearing the wedding ring, now the Captain is angry;
	move player to Comm Space.


Chapter 3 - The Galley

The Galley is a room.  "A room for stainless steel and fire.  There are scorch mark behind and above the hob.  There's a passageway to the north leading to the Bridge."  The Galley is south of the bridge.

The Scorch Marks are scenery in the Galley.  Understand "steel" and "stainless steel" as Scorch Marks.  The description is "Char and soot coat the cooktop surfaces.  Nothing out of place, but that cannot be sanitary."

The Passageway To Bridge is scenery in Galley.  The description is "A passageway to the north leads to the bridge."

The vending machine is a closed container and locked.  The vending machine is in the Galley.  The description is "A large metal box with a polycarbonate viewing window.  Fully stocked with palatable snacks.[if the candy bar is in the vending machine]  A candy bar is stuck in the coil, leaning against the window."

The window is scenery in the galley.  Understand "vending machine window" as the window.  The description is "Polycarbonate and hard as nails.[if the candy bar is in the vending machine]  A candy bar is stuck in the coil, leaning against the window."

The candy bar is a thing.  The description is "Chewy and chock full of energy.  Uses the latest hydrocarbon matrix of maple and oats."

[ duplicate logic for two items ]
Before examining the vending machine:
	now the vending machine is transparent;
	if the candy bar is nowhere:
		now the candy bar is in the vending machine.
Before examining the window:
	now the vending machine is transparent;
	if the candy bar is nowhere:
		now the candy bar is in the vending machine.

Instead of attacking the window:
	say "The window is stronger than you, but you succeed in rocking the machine.  [run paragraph on]";
	try attacking the vending machine.

Before attacking the vending machine:
	if the candy bar is in the vending machine or the candy bar is nowhere:
		say "The candy bar breaks free and kafuffles out the chute and onto the floor.";
		now the candy bar is in the galley;
		stop the action;
	otherwise:
		say "You rattle the machine and it's contents.";
		stop the action.

Before taking candy bar:
	if the candy bar is in the vending machine:
		say "You try to reach through the chute, but the candy bar is out of reach.";
		stop the action.

After taking candy bar:
	say "The wrapper crinkles at your touch.  Taken."

Instead of eating the candy bar:
	say "You nibble on the corner.  It's tasteless and stale.  This is clearly not food for humans anymore."


Chapter 4 -The Engine Room

The Engine Room is a room.  "Endless rows of pipes and valves.  The room is covered in grease and dirt, but is the tidiest room you've seen.[if the warp drive is humming]  The warp drive humming along.  An acrid smell is coming from the tank.[end if]  As you move around, some lights flicker to life.  The room is still dim and there are many dark corners."  It is south of the hallway.

The Warp Drive is a thing.  The drive is in the Engine Room.  The warp drive can be humming or silent.  The drive is silent.  The description is "A hulking mass of stains and rust.  On it, a panel is faintly glowing.  The engine has a large tank that is little more than a tin can."  It is fixed in place.

A tank is a container.  A tank is part of the drive.  The tank can be empty or full.  The tank is empty.  Understand "tin can/can/tanks" as the tank.  The description is "This archaic peice of tech is little more than a tin can, a hopper to feed the machine."

A panel is part of the drive.  The description is "The panel has what looks like a canister symbol [if the tank is full]that's mostly filled in.[otherwise]that's pulsing with a very small red sliver."

Before attacking the tank:
	say "[if the tank is full]You hear a rumbling thwong.[otherwise]You hear a hollow ping.";
	stop the action

Instead of using the candy bar:
	if player is in the engine room:
		try inserting the candy bar into the tank;
	otherwise:
		say "Nothing happens."

After inserting into the tank:
	now the tank is full;
	now the candy bar is nowhere;
	now the melted bar is in the tank;
	say "The panel blinks and chimes a self-satisfied tone."

The melted bar is a thing.  The description is "Chewy and emanating a faint radioactive glow.[if the warp drive is humming]  It's crisping at the edges."  The melted bar is nowhere.

Before taking the melted bar:
	say "The melted bar is too hot to touch.";
	stop the action.

The Engine Room Lights are scenery in the Engine Room.  The description is "They mostly hold their soft glow, but they flicker every once in a while.  The shadows dance about as the lights flicker."

The Engine Room Pipes are scenery in the Engine Room.  The description is "Rounds and rounds of tubes and pipes.  This is a complex machine."

The Engine Room Valves are scenery in the Engine Room.  The description is "Handles and spigots.  They are color coded and numerous."

The Engine Room Dirt is scenery in the Engine Room.  Understand "grease" as dirt.  The description is "The room is covered in grease and dirt, but is the tidiest room you've seen."

Shadows are scenery in the Engine Room.  Understand "dark" and "corners/corner" as shadows.  The description is "The shadows dance about as the lights flicker.  Looking at them plays tricks on your eyes, but you see nothing out of place.[if the ring is nowhere]..  Wait.  What is this ring doing here?"

After examining the shadows:
	if the wedding ring is nowhere:
		say "You pick up the ring.  It's covered in soot from some kind of electrical spark.  You polish it to a shine and put it in your pocket.";
		now the player is carrying the wedding ring.


Chapter 5 - The Hallway

The Hallway is a room.  "The detritus makes this cramped space feel even smaller.  Some of the floor panels have been overturned.  Two small corridors bend out of view, one to the north and one to the south."  The hallway is north of the engine room and south of the galley.

The Crew Cabin Door is a closed door.  The description is "Some debris has fallen around the door.  It looks bent out of shape."  The crew cabin door is west of The Hallway.

Instead of attacking the crew cabin door:
	say "You vent your frustration on the crew cabin door, but it does not yield."

Instead of opening the crew cabin door:
	say "You try to break it free, but it's jammed into place."

Detritus is scenery in the hallway.  Understand "debris/bits/bobs/papers/guns" as Detritus.  The description is "Bits and bobs, papers and guns, and bloodstains.  Bloodstains everywhere."

Crannies are scenery in the hallway.  Understand "nook/nooks/cavities/cavity/cranny/hold" as crannies.  The description is "The smugglers hold has been upended.  Someone was looking for something."

Floor Panels are scenery in the hallway.  Understand "panel" as panels.  The description is "Lots of hidden crannies have been revealed.  Someone was looking for something."


Chapter 6 - Comm Space

[TODO comminicate with captain about ship => you can't see any such thing -- i mean, you are on a ship]
Comm Space is a room.  The description is "[if unvisited]Almost by habit, your fingers key in a barely remembered code.  There is a shimmer, then the holographic form of the Captain blinks into view.[otherwise]The figure of the Captain appears, almost as if she were anticipating your return.[end if][if the Captain is angry]  Her eyes flash angrily with recognition."

A blue button is in Comm Space.  The description is "The Cancel button pulses softly, almost daring you to press it."

Instead of pushing the blue button:
	say "The comm stars wink out as the connection closes.  You step back out into the Bridge.";
	move player to the Bridge.

The Captain is a woman in Comm Space.  The description is "Tall and slender, the Captain's stern face reveals her many years travelling the star lanes.[if the Captain is angry]  Her mouth is twisted into an angry scowl.[otherwise if the Captain is forgiving]  Her eyes look toward you expectantly, her hands reaching out."

The Captain's hands are a part of the Captain.  The description is "[if the Captain is forgiving]Her palms are open, reaching out to you expectantly.[otherwise]The Captain's fists are clenched in anger."  Understand "hand/palms/palm/fists/fist" as the Captain's hands.

Instead of taking the Captain's hands:
	try kissing the Captain.

Mood is a kind of value.  The moods are angry, stern, and forgiving.
The Captain has a mood.  The Captain is angry.

A person can be repentant or unrepentant.  The player is unrepentant.  The Captain is unrepentant.

Apologizing is an action applying to one visible thing.
Understand "apologize [someone]" as apologizing.
Understand "apologize to[someone]" as apologizing.
Understand "say sorry to[someone]" as apologizing.

Instead of apologizing, try saying sorry.

Instead of saying sorry:
	if the player is in Comm Space:
		if the player is unrepentant:
			say "Her eyes widen in poorly masked surprise.  'You?  Apologize?  Well! I...  I accept.'  Her mood softens somewhat.";
			now the player is repentant;
			if the Captain is angry:
				now the Captain is stern;
			otherwise if the Captain is stern:
				now the Captain is forgiving;
		otherwise:
			say "She sniffs.  'You will need to do better than just repeat yourself.'"

Instead of asking someone to try saying sorry, try saying sorry.

The wedding ring is a wearable thing.  The description is "An ornate wedding ring, crafted of some undescribably rare precious metal.  Even though it must be priceless, multiple pits and scratches show that it has been treated rather thoughtlessly.  A dim memory reminds you of...  someone.[if player is wearing the wedding ring]  It fits your finger perfectly."

After wearing the wedding ring:
	say "It fits perfectly."

Instead of showing the wedding ring to the Captain:
	if the player is in Comm Space:
		if the wedding ring is worn:
			if the Captain is unrepentant:
				say "A wry smile flits across her face.  'So...  I suppose that means something to you after all.'  Her mood softens somewhat.";
				now the Captain is repentant;
				if the Captain is angry:
					now the Captain is stern;
				otherwise if the Captain is stern:
					now the Captain is forgiving;
			otherwise:
				say "'I see that.  But do you have it within you to apologize for your foolishness?'";
		otherwise:
			say "The Captain is nonplussed.  'I don't see that on your finger.'"

[BUG if the player enters "captain," the game crashes]
Instead of asking the Captain to try doing something:
	let T be "[the player's command]";
	replace the regular expression "\w+," in T with "";
	change the text of the player's command to T;
	repeat through Table of Captain's Commentary:
		if player's command includes topic entry:
			if the Captain is forgiving:
				say "[one of]The Captain says nothing.  Her eyes look to you expectantly.[or]Instead of replying, she smiles slightly, her  palms open.[or]The Captain arches an eyebrow invitingly.  'Well?'[cycling]";
				rule succeeds;
			otherwise:
				say "[commentary entry][paragraph break]";
				rule succeeds;
	say "The Captain sniffs disdainfully." [TODO this statement is unreachable, or i can't find an example]

Talking to is an action applying to one visible thing.
Understand "talk [someone]" as talking.
Understand "talk to [someone]" as talking.
Understand "communicate [someone]" as talking.
Understand "communicate with [someone]" as talking.
[TODO Understand "talk to [someone] about [something]" as asking it about the subject.]
Instead of talking to the Captain:
	say "Were you going to ask the Captain about something?"


Understand "ask [someone] for help" as talking.

Answering someone that something is speech.
Asking someone about something is speech.
Asking someone for something is speech.
Talking to someone is speech.
Telling someone about something is speech.

Instead of speech when the noun is Captain:
	repeat through Table of Captain's commentary:
		if the topic understood includes topic entry:
			if the Captain is forgiving:
				say "[one of]The Captain says nothing.  Her eyes look to you expectantly.[or]Instead of replying, she smiles slightly, her  palms open.[or]The Captain arches an eyebrow invitingly.  'Well?'[cycling]";
				rule succeeds;
			otherwise:
				say "[commentary entry][paragraph break]";
				rule succeeds;
	say "The Captain arches her eyebrow."

[TODO work on vocabulary]
[TODO "what thave I done" "what did I do"]
Table of Captain's Commentary
topic		commentary
 "help"	"'So, begging for help, are we?  That is rich considering who caused the explosion in the first place.'"
 "explosion"	"'You really don't remember, do you?  Our best pilot...  without his memory.'"
 "me/myself"	"'To think that I once loved you...  And now it has come to this.'"
 "Captain/herself/you"	"'This must be the first time since we got married that you have shown any concern for me.'"
 "love/marriage/married/wife"	"'Show me the wedding ring I risked life and limb for, then we can talk about this.'"
 "gurney"	"'Well, we couldn't leave you to die...  though I suppose that we did anyway.'"
 "pilot"	"'You pushed the engines too hard, and you cost me my ship!'  She checks her breath.  'And you never even said that you were sorry...'"
 "sorry"	"'Could you?  Could you really apologize?'"
 "warp/engine/drive"	"The captain sighs.  'How could you?  Running the engine hot is one thing, but to risk all our lives by trying to modify it for even more?'  She shakes her head.  'I don't know why I ever trusted you.'"
 "trust"	"'You speak of trust?  Prove to me that you have learned something from all this, then we can speak of trust.'"
 "ring"	"'I raided a dozen treasure craft to find that ring.  The day you accepted it was the best day of my life.  But you seem to have discarded it the same way you discarded our lives.  Where is it?  Show me.'"
"memory/what/happend"	"She looks at you with an odd expression.  'You took quite a shock to your system...  though I daresay you deserved it.  You were lucky we got you strapped to that gurney in time.  It looks like it wasn't a complete recovery, was it?'"
"ship"	"A sad expression crosses her face.  'My ship...  look what you have done to it!'"
"proof/prove"	"She shakes her head.  'An apology is a start.  After that?' Her voice trails off as she shakes her head again.  Find what you lost."

Instead of kissing the Captain:
	if the player is in Comm Space:
		if the Captain is not forgiving:
			say "The captain leans in close, and even though it is just a holographic image, you feel the heat of the Captain's ire as she slams the Cancel button.";
			try pushing the blue button;
		otherwise:
			if the player is not wearing the wedding ring:
				say "The captain sighs, 'When will you ever learn...'  She takes the ring and puts it in her pocket.";
			end the story saying "Somehow, the Captain feels your embrace through the holographic connection.  Her stern visage melts.  'We never should have left you behind...  Hold fast my love, I am on my way!'  True to her word, her craft rescues you two long days later.  You resume your place at her side...  but how long can you remain?"


Chapter 7 - Testing - Not for release

[
The Descriptionless Test Banana is a thing.
When play begins:
	say "Checking for items without descriptions...";
	repeat with item running through things:
		if description of the item is "":
			say " - [item] has no description."
]


Chapter 8 - Notes

[
Backstory

You are the pilot of a small smuggling ship.
Your wife is the captain.

Some untold antagonist caught up to you.
You were hot dogging to get away, it didn't work, and the ship got captured.
You probably lost control and ran into something, the front of the ship is more damaged than the back.  The hallway has been raided.
The crew strapped you to the gurney and left you stranded.
These antagonists boarded, and stole your goods.

Now you need to get the ship into working order.
Contact your wife, appologize.
]

[
Theme

The start of the game is somber.  It looks like everyone is dead.
Somebody messed up, and your just trying to get things back together.
Everything is dimmly lit and lonely.
You can find clues to piece the situation together, but it isn't direct.

Once you contact your wife, the story has an explosion of color.  And all is revealed.
One final trial before you can go home.
]

[ General Problems
 - very narrow set of actions "go, look, examine, "put x in y"
]
